package com.example.equipmentrentalapplication.domain;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class RentalSheet {

  // should show the error messages, somehow doesn't work
  @NotBlank(message = "You must fill this field to rent an equipment!")
  private String firstName;

  @NotBlank(message = "You must fill this field to rent an equipment!")
  private String lastName;

  @NotBlank(message = "You must fill this field to rent an equipment!")
  @Email
  private String eMailAdress;

  @NotBlank(message = "Please choose an equipment you would like to rent!")
  private String equipmentType;

  @NotNull(message = "Please fill out this field!")
  @Positive(message = "Please use valid values (positive integers)!")
  private Integer duration;

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String geteMailAdress() {
    return eMailAdress;
  }

  public void seteMailAdress(String eMailAdress) {
    this.eMailAdress = eMailAdress;
  }

  public String getEquipmentType() {
    return equipmentType;
  }

  public void setEquipmentType(String equipmentType) {
    this.equipmentType = equipmentType;
  }

  public Integer getDuration() {
    return duration;
  }

  public void setDuration(Integer duration) {
    this.duration = duration;
  }

  // this doesn't work either. maybe a conflict in the solutions?
  public RentalSheet(@NotNull(message = "You must fill this field to rent an equipment!") String firstName,
      @NotNull(message = "You must fill this field to rent an equipment!") String lastName,
      @NotNull(message = "You must fill this field to rent an equipment!") @Email String eMailAdress,
      @NotNull(message = "Please choose an equipment you would like to rent!") String equipmentType,
      @NotNull(message = "Please fill out this field!") @Positive(message = "Please use valid values (positive integers)!") Integer duration) {
    super();
    this.firstName = firstName;
    this.lastName = lastName;
    this.eMailAdress = eMailAdress;
    this.equipmentType = equipmentType;
    this.duration = duration;
  }

  @Override
  public String toString() {
    return "RentalSheet [firstName=" + firstName + ", lastName=" + lastName + ", eMailAdress=" + eMailAdress
        + ", equipmentType=" + equipmentType + ", duration=" + duration + "]";
  }

}
