package com.example.equipmentrentalapplication.service.impl;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.equipmentrentalapplication.domain.RentalSheet;
import com.example.equipmentrentalapplication.repository.EquipmentRentalRepository;
import com.example.equipmentrentalapplication.service.RentalFormService;

@Service
public class RentalFormServiceImpl implements RentalFormService {
	
	@Autowired
	EquipmentRentalRepository equipmentRentalRepository;

	@Override
	public void registerNewForm(@NotNull @Valid RentalSheet rentalSheet) {
		equipmentRentalRepository.save(rentalSheet);
	}

}
