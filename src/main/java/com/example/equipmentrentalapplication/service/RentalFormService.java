package com.example.equipmentrentalapplication.service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.example.equipmentrentalapplication.domain.RentalSheet;

@Validated
public interface RentalFormService {

	void registerNewForm(@NotNull @Valid RentalSheet rentalSheet);
}
