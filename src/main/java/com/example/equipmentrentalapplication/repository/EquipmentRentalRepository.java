package com.example.equipmentrentalapplication.repository;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.example.equipmentrentalapplication.domain.RentalSheet;

@Validated
public interface EquipmentRentalRepository {
	
	void save(@NotNull @Valid RentalSheet rentalSheet);

}
