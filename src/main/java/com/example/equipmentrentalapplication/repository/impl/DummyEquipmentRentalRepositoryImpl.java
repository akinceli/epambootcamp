package com.example.equipmentrentalapplication.repository.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.example.equipmentrentalapplication.domain.RentalSheet;
import com.example.equipmentrentalapplication.repository.EquipmentRentalRepository;

@Repository
public class DummyEquipmentRentalRepositoryImpl implements EquipmentRentalRepository {

  private Logger logger = LoggerFactory.getLogger(getClass());

  // to be implemented in a database
  @Override
  public void save(RentalSheet rentalSheet) {
    logger.info("'save' {} now..." + rentalSheet);

  }

}
