package com.example.equipmentrentalapplication.repository.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.example.equipmentrentalapplication.repository.AvailableEquipmentRepository;

@Repository
public class AvailableEquipmentRepositoryImpl implements AvailableEquipmentRepository {

  private static List<String> equipmentList;

  // Static constructor --> make the list only once. For now it's hard wired.
  static {
    equipmentList = new ArrayList<>();
    equipmentList.add("Sailing boat");
    equipmentList.add("Jetski");
    equipmentList.add("Motor boat");
  }

  // Implementing the interface
  @Override
  public List<String> getEquipmentList() {
    return equipmentList;
  }

}
