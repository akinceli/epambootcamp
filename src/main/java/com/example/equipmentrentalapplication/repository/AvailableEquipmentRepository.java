package com.example.equipmentrentalapplication.repository;

import java.util.List;

public interface AvailableEquipmentRepository {

  List<String> getEquipmentList();
}
