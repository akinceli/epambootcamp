package com.example.equipmentrentalapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EquipmentrentalapplicationApplication {

	public static void main(String[] args) {
		SpringApplication.run(EquipmentrentalapplicationApplication.class, args);
	}
}
