package com.example.equipmentrentalapplication.controller;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.equipmentrentalapplication.domain.RentalSheet;
import com.example.equipmentrentalapplication.repository.AvailableEquipmentRepository;
import com.example.equipmentrentalapplication.service.RentalFormService;

@Controller
public class RentalFormController {

  @Autowired
  RentalFormService rentalFormService;

  @Autowired
  AvailableEquipmentRepository availableEquipmentRepository;

  // validation not working
  @RequestMapping(value = "/rent-an-equipment", method = RequestMethod.GET)
  public String getEquipmentRentalForm(@ModelAttribute("rentalSheet") RentalSheet rentalSheet, Model model) {
    model.addAttribute("typeOfEquipments", availableEquipmentRepository.getEquipmentList());
    return "equipment-rental-form";
  }

  @RequestMapping(value = "/rental-form", method = RequestMethod.POST)
  public String rentalFromControllerMethod(@ModelAttribute("rentalSheet") @Valid RentalSheet rentalSheet, Model model,
      BindingResult result, HttpServletResponse response) {

    if (result.hasErrors()) {
      response.setStatus(HttpStatus.BAD_REQUEST.value());
      model.addAttribute("typeOfEquipments", availableEquipmentRepository.getEquipmentList());
      result.reject("subscriptionForm.error.incompleteInput");
      return "equipment-rental-form";
    } else {
      rentalFormService.registerNewForm(rentalSheet);
      return "redirect:/thank-you";
    }
  }

  // if the data is correct goes well
  @RequestMapping(value = "/thank-you", method = RequestMethod.GET)
  public String getConfirmationPageIfRentalIsSuccessful() {
    return "confirmation";
  }
}
